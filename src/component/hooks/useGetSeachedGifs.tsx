import { useQuery } from "@tanstack/react-query";
import axios from "axios";
import { API_URL, YOUR_API_KEY } from "../service/config";

export const useGetSeachedGifs = (query: string) => {
  return useQuery(["getSearchedGifs", query], async () => {
    if (query) {
      const response = await axios.get(`${API_URL}/search`, {
        params: {
          api_key: YOUR_API_KEY,
          q: query,
          limit: 50,
        },
      });
      return response.data.data;
    }
    return [];
  });
};
