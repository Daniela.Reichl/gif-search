import { useQuery } from "@tanstack/react-query";
import axios from "axios";
import { API_URL, YOUR_API_KEY } from "../service/config";

export const useGetAllGifs = () => {
  return useQuery({
    queryKey: ["getAllGifs"],
    queryFn: async () => {
      const response = await axios.get(`${API_URL}/trending`, {
        params: {
          api_key: YOUR_API_KEY,
          limit: 50,
        },
      });
      return response.data.data;
    },
  });
};
