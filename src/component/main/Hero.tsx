import { useState } from "react";
import { useQueryClient } from "@tanstack/react-query";
import { useGetSeachedGifs } from "../hooks/useGetSeachedGifs";
import { HeroProps } from "../types/types";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import { useForm } from "react-hook-form";

// yup validation
const schema = yup.object().shape({
  search: yup.string().required("Gib einen Suchbegriff ein"),
});

export const Hero = ({ setFetchedData }: HeroProps) => {
  const [searchQuery, setSearchQuery] = useState("");
  const queryClient = useQueryClient();

  const searchedGifs = useGetSeachedGifs(searchQuery);

  const handleSearch = async () => {
    await queryClient.invalidateQueries(["getSearchedGifs", searchQuery]);
    setFetchedData(searchedGifs.data);
  };

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
  });

  return (
    <div className="flex flex-col gap-2 justify-center items-center h-60 bg-[url('/src/assets/images/background.jpg')] bg-slate-700 bg-center bg-cover font-bold ">
      <form
        onSubmit={handleSubmit(handleSearch)}
        className="flex gap-5 items-center"
      >
        <input
          type="text"
          {...register("search")}
          placeholder="Gif"
          className="bg-violet-800 border-2 text-white border-slate-500 bg-opacity-80 placeholder:text-slate-500 rounded-full py-2 px-5"
          value={searchQuery}
          onChange={(event) => setSearchQuery(event.target.value)}
        />
        <button className="buttonStyle" type="submit">
          Search
        </button>
      </form>
      <p className="text-yellow-500 ">{errors.search?.message}</p>
    </div>
  );
};
