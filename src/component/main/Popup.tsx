import { useState } from "react";
import { GifType, PopupProps } from "../types/types";

export const Popup = ({ selectedGif, closeModal }: PopupProps) => {
  const [message, setMessage] = useState<string>("");

  // safe gif in local storage
  const saveGif = (selectedGif: GifType) => {
    const savedGifs = JSON.parse(localStorage.getItem("savedGifs") || "[]");
    savedGifs.push(selectedGif.images.original.url);
    localStorage.setItem("savedGifs", JSON.stringify(savedGifs));
    setMessage("Gif wurde zu deinen Favoriten hinzugefügt");
  };

  return (
    <div>
      {selectedGif && (
        <div className="flex flex-col gap-5 justify-centers items-center">
          <p className="text-white p-2 w-fit text-center font-bold text-xl hover:text-violet-400 hover:underline">
            {selectedGif.title}
          </p>
          <img src={selectedGif.images.original.url} alt={selectedGif.title} />
          <button
            className="rounded-full py-2 px-5 bg-violet-600 top-5 right-5 w-fit flex gap-2"
            onClick={() => saveGif(selectedGif)}
          >
            <img
              src="/src/assets/icons/heart.svg"
              alt="heart"
              className="h-6"
            />
            Zu Favoriten hinzufügen
          </button>
          <p>{message}</p>
        </div>
      )}
      <img
        src="../src/assets/icons/close.svg"
        alt="close"
        className="absolute rounded-full h-12 p-2 bg-violet-600 top-5 right-5 hover:scale-105 hover:bg-gradient-to-tr from-violet-600 to-violet-900 cursor-pointer"
        onClick={closeModal}
      />
    </div>
  );
};
