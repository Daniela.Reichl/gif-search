import { useState } from "react";
import Modal from "react-modal";
import { GifsCard } from "./GifsCard";
import { Popup } from "./Popup";
import { GifType, GifsviewProps } from "../types/types";
import { useGetAllGifs } from "../hooks/useGetAllGifs";

export const Gifsview = ({ fetchedData }: GifsviewProps) => {
  const [selectedGif, setSelectedGif] = useState<GifType | null>(null);

  const { data: allGifsData, isLoading, isError } = useGetAllGifs();

  const [isModalOpen, setIsModalOpen] = useState(false);

  // open popup if a gif is clicked
  const handleGifClick = (gif: GifType) => {
    setSelectedGif(gif);
    setIsModalOpen(true);
  };

  // close popup
  const closeModal = () => {
    setSelectedGif(null);
    setIsModalOpen(false);
  };

  if (isLoading) {
    return <div>Loading...</div>;
  }

  if (isError) {
    return <div>Error fetching GIFs</div>;
  }

  return (
    <div className="mainBase">
      <div className="flex flex-wrap gap-10 px-20 py-10 justify-center">
        {fetchedData
          ? fetchedData.map((gif: GifType) => (
              <GifsCard
                key={gif.id}
                gif={gif}
                handleGifClick={handleGifClick}
              />
            ))
          : allGifsData.map((gif: GifType) => (
              <GifsCard
                key={gif.id}
                gif={gif}
                handleGifClick={handleGifClick}
              />
            ))}
      </div>
      <Modal
        isOpen={isModalOpen}
        onRequestClose={closeModal}
        className="flex justify-center items-center h-full bg-slate-900 bg-opacity-70 relative text-white"
      >
        <Popup selectedGif={selectedGif} closeModal={closeModal} />
      </Modal>
    </div>
  );
};
