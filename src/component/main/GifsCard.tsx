import { GifsCardProp } from "../types/types";

export const GifsCard = ({ gif, handleGifClick }: GifsCardProp) => {
  return (
    <div key={gif.id}>
      <div className="flex flex-col gap-3 items-center">
        <img
          src={gif.images.fixed_height.url}
          alt={gif.title}
          className="self-center w-fit hover:scale-105 shadow-xl rounded-md"
          onClick={() => handleGifClick(gif)}
        />
      </div>
    </div>
  );
};
