import { useState } from "react";
import { Hero } from "./Hero";
import { Gifsview } from "./Gifsview";
import { GifType } from "../types/types";

export const Homeview = () => {
  const [fetchedData, setFetchedData] = useState<GifType[] | null>(null);

  return (
    <div>
      <Hero setFetchedData={setFetchedData} />
      <Gifsview fetchedData={fetchedData} />
    </div>
  );
};
