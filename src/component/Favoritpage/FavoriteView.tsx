import { useEffect, useState } from "react";
import deleteIcon from "/src/assets/icons/delete.svg";

export const FavoriteView = () => {
  const [savedGifs, setSavedGifs] = useState<string[]>([]);

  // loade gifs from local storage
  useEffect(() => {
    const savedGifsFromLocalStorage = JSON.parse(
      localStorage.getItem("savedGifs") || "[]"
    );
    setSavedGifs(savedGifsFromLocalStorage);
  }, []);

  // Remove the GIF URL at the clicked index
  const handleDeleteClick = (index: number) => {
    const updatedSavedGifs = [...savedGifs];
    updatedSavedGifs.splice(index, 1);
    setSavedGifs(updatedSavedGifs);
    localStorage.setItem("savedGifs", JSON.stringify(updatedSavedGifs));
  };

  return (
    <div className="mainBase">
      <div className="base flex-col items-center">
        <h1 className="font-bold text-violet-200 text-5xl">Meine Favoriten</h1>
        <div className="base">
          {savedGifs.map((gifUrl, index) => (
            <div className="relative hover:scale-105">
              <img key={index} src={gifUrl} alt={`Saved GIF ${index}`} />
              {/* delete gifs from local storage */}
              <img
                src={deleteIcon}
                alt="delete"
                className="absolute top-2 right-2 h-12 bg-violet-600 rounded-full p-2 cursor-pointer hover:scale-105"
                onClick={() => handleDeleteClick(index)}
              />
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};
