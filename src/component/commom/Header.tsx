import { Link } from "react-router-dom";
import logo from "/src/assets/icons/gif-logo.svg";
import heart from "/src/assets/icons/heart.svg";

export const Header = () => {
  return (
    <div className="py-3 px-5 flex justify-between items-center bg-gradient-to-r from-violet-800 to-slate-800 text-violet-200 font-bold">
      {/* Link to homepage */}
      <Link to="/" className="flex gap-2 items-center">
        <img src={logo} alt="logo" className="h-12 hover:scale-105" />
        <p className="hover:text-violet-400">GIF`s für alle</p>
      </Link>
      {/* Link to favorite page */}
      <Link to="favorits" className="flex gap-2 items-center">
        <img src={heart} alt="heart" className="h-6" />
        <p className="hover:text-violet-400"> Meine Favoriten</p>
      </Link>
    </div>
  );
};
