// Gifs

export type GifType = {
  type: string;
  id: string;
  url: string;
  slug: string;
  bitly_gif_url: string;
  bitly_url: string;
  embed_url: string;
  username: string;
  source: string;
  title: string;
  images: {
    original: {
      url: string;
    };
    fixed_height: {
      url: string;
    };
  };
};

// props

export type GifsviewProps = {
  fetchedData: GifType[] | null;
};

export type GifsCardProp = {
  gif: GifType;
  handleGifClick: (gif: GifType) => void;
};

export type HeroProps = {
  setFetchedData: React.Dispatch<React.SetStateAction<GifType[] | null>>;
};

export type PopupProps = {
  selectedGif: GifType | null;
  closeModal: (event: React.MouseEvent<HTMLImageElement>) => void;
};
