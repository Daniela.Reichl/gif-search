Quick React Project Setup

This repository contains a minimal React project setup to help you get started quickly.

Installation

To install the project dependencies, run:
npm install

Running the Project

To start the development server, use:
npm run dev

The project will be available at your local host by default.

Have fun!
